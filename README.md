[![pipeline status](https://gitlab.com/jpimag/7-segment/badges/master/pipeline.svg)](https://gitlab.com/jpimag/7-segment/commits/master)
[![coverage report](https://gitlab.com/jpimag/7-segment/badges/master/coverage.svg)](https://jpimag.gitlab.io/7-segment/coverage)

# 7-segment

Implementation of a 7 segment display in java

## Requirement

Jdk 8+

Maven 3.5+

## Usage

```
java -jar dist/7segment.jar $numbers
```

where $numbers is a suite of digit [0-9]
