package org.jpimag.segment7;

import java.util.List;

import org.jpimag.segment7.model.Segment7;

public class Main {

	/**
	 * Display first argument in 7-segment way
	 * 
	 * @param args first argument is required
	 */
	public static void main(String[] args) {

		try {
			// input
			if (args.length == 0) {
				throw new IllegalArgumentException("Error : one argument is expected");
			}
			String input = args[0];

			// Convert from string
			List<Segment7> segment7s = new Segment7StringConvertor().fromString(input);

			// Display
			String output = new Segment7Displayer().toDisplay(segment7s);
			System.out.print(output);

		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}

}
