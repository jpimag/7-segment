package org.jpimag.segment7;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jpimag.segment7.model.Segment7;

/**
 * 
 * Implement the 7-segment display of a {@link Segment7}
 */
public class Segment7Displayer {

	private static final String LINE_SEPARATOR = "\n";
	private static final String SPACE = " ";
	private static final String DASH = "_";
	private static final String PIPE = "|";

	/**
	 * 
	 * @param segment7s a list of {@link Segment7} to display
	 * @return a {@link String} representing the display in 7-segment way
	 */
	public String toDisplay(List<Segment7> semgent7s) {
		String line1 = toDisplayLine(semgent7s, this::toDisplayLine1);
		String line2 = toDisplayLine(semgent7s, this::toDisplayLine2);
		String line3 = toDisplayLine(semgent7s, this::toDisplayLine3);
		return Stream.of(line1, line2, line3).collect(Collectors.joining(LINE_SEPARATOR));
	}

	private String toDisplayLine(List<Segment7> semgent7s, Function<Segment7, Stream<String>> lineFunction) {
		return semgent7s.stream().map(lineFunction).flatMap(s -> s).collect(Collectors.joining());
	}

	private Stream<String> toDisplayLine1(Segment7 segment7) {
		return Stream.of(SPACE, dash(segment7.isTop()), SPACE);
	}

	private Stream<String> toDisplayLine2(Segment7 segment7) {
		return Stream.of(pipe(segment7.isTopLeft()), dash(segment7.isMiddle()), pipe(segment7.isTopRight()));
	}

	private Stream<String> toDisplayLine3(Segment7 segment7) {
		return Stream.of(pipe(segment7.isBottomLeft()), dash(segment7.isBottom()), pipe(segment7.isBottomRight()));
	}

	private String dash(boolean is) {
		return is ? DASH : SPACE;
	}

	private String pipe(boolean is) {
		return is ? PIPE : SPACE;
	}
}
