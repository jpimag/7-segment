package org.jpimag.segment7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jpimag.segment7.model.Segment7;
import org.jpimag.segment7.model.Segment7Characters;

/**
 * Convert a {@link String} to a list of {@link Segment7}.
 */
public class Segment7StringConvertor {

	/**
	 * Map of correspondant of {@link Segment7} by character
	 */
	private static final Map<Character, Segment7> SEGMENT7_CHARACTERS_MAP;

	static {
		// Initialisation of SEGMENT7_CHARACTERS_MAP
		Map<Character, Segment7> map = new HashMap<>();
		map.put('0', Segment7Characters.ZERO);
		map.put('1', Segment7Characters.ONE);
		map.put('2', Segment7Characters.TWO);
		map.put('3', Segment7Characters.THREE);
		map.put('4', Segment7Characters.FOUR);
		map.put('5', Segment7Characters.FIVE);
		map.put('6', Segment7Characters.SIX);
		map.put('7', Segment7Characters.SEVEN);
		map.put('8', Segment7Characters.HEIGHT);
		map.put('9', Segment7Characters.NINE);
		SEGMENT7_CHARACTERS_MAP = Collections.unmodifiableMap(map);
	}

	/**
	 * @param s a {@link String} to convert
	 * @return a list of {@link Segment7} among {@link Segment7Characters} constants
	 * @throws IllegalArgumentException if the string contains a non accepted
	 *                                  character
	 */
	public List<Segment7> fromString(String s) {
		List<Segment7> segment7s = new ArrayList<>();
		// Iterate over input characters
		for (char c : s.toCharArray()) {
			Character character = Character.valueOf((char) c);

			// Search Segment7 character in map
			if (SEGMENT7_CHARACTERS_MAP.containsKey(character)) {
				// character accepted
				segment7s.add(SEGMENT7_CHARACTERS_MAP.get(character));
			} else {
				// character not accepted
				throw new IllegalArgumentException(
						String.format("Only %s characters are allowed for 7-segment display", SEGMENT7_CHARACTERS_MAP.keySet()));
			}
		}
		return segment7s;
	}

}
