package org.jpimag.segment7.model;

/**
 * Enum representing the 7 segments
 * 
 */
public enum SegmentEnum {
	TOP,
	TOP_LEFT,
	TOP_RIGHT,
	MIDDLE,
	BOTTOM_LEFT,
	BOTTOM_RIGHT,
	BOTTOM;
}
