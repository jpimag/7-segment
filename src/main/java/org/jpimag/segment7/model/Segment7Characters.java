package org.jpimag.segment7.model;

/**
 * 7-segment known characters
 * 
 */
public final class Segment7Characters {

	public static final Segment7 ZERO = new Segment7(
			SegmentEnum.TOP,
			SegmentEnum.TOP_LEFT,
			SegmentEnum.TOP_RIGHT,
			SegmentEnum.BOTTOM_LEFT,
			SegmentEnum.BOTTOM_RIGHT,
			SegmentEnum.BOTTOM);

	public static final Segment7 ONE = new Segment7(
			SegmentEnum.TOP_RIGHT,
			SegmentEnum.BOTTOM_RIGHT);

	public static final Segment7 TWO = new Segment7(
			SegmentEnum.TOP,
			SegmentEnum.TOP_RIGHT,
			SegmentEnum.MIDDLE,
			SegmentEnum.BOTTOM_LEFT,
			SegmentEnum.BOTTOM);

	public static final Segment7 THREE = new Segment7(
			SegmentEnum.TOP,
			SegmentEnum.TOP_RIGHT,
			SegmentEnum.MIDDLE,
			SegmentEnum.BOTTOM_RIGHT,
			SegmentEnum.BOTTOM);

	public static final Segment7 FOUR = new Segment7(
			SegmentEnum.TOP_LEFT,
			SegmentEnum.TOP_RIGHT,
			SegmentEnum.MIDDLE,
			SegmentEnum.BOTTOM_RIGHT);

	public static final Segment7 FIVE = new Segment7(
			SegmentEnum.TOP,
			SegmentEnum.TOP_LEFT,
			SegmentEnum.MIDDLE,
			SegmentEnum.BOTTOM_RIGHT,
			SegmentEnum.BOTTOM);

	public static final Segment7 SIX = new Segment7(
			SegmentEnum.TOP,
			SegmentEnum.TOP_LEFT,
			SegmentEnum.MIDDLE,
			SegmentEnum.BOTTOM_LEFT,
			SegmentEnum.BOTTOM_RIGHT,
			SegmentEnum.BOTTOM);

	public static final Segment7 SEVEN = new Segment7(
			SegmentEnum.TOP,
			SegmentEnum.TOP_RIGHT,
			SegmentEnum.BOTTOM_RIGHT);

	public static final Segment7 HEIGHT = new Segment7(
			SegmentEnum.TOP,
			SegmentEnum.TOP_LEFT,
			SegmentEnum.TOP_RIGHT,
			SegmentEnum.MIDDLE,
			SegmentEnum.BOTTOM_LEFT,
			SegmentEnum.BOTTOM_RIGHT,
			SegmentEnum.BOTTOM);

	public static final Segment7 NINE = new Segment7(
			SegmentEnum.TOP,
			SegmentEnum.TOP_LEFT,
			SegmentEnum.TOP_RIGHT,
			SegmentEnum.MIDDLE,
			SegmentEnum.BOTTOM_RIGHT);

	private Segment7Characters() {
	}

}
