package org.jpimag.segment7.model;

import java.util.Arrays;
import java.util.EnumSet;

/**
 * A 7-segment character
 */
public class Segment7 {

	/**
	 * Set of displayed segments
	 */
	private EnumSet<SegmentEnum> segments;

	public Segment7(SegmentEnum... segments) {
		super();
		this.segments = EnumSet.noneOf(SegmentEnum.class);
		this.segments.addAll(Arrays.asList(segments));
	}

	public boolean isTop() {
		return contains(SegmentEnum.TOP);
	}

	public boolean isTopLeft() {
		return contains(SegmentEnum.TOP_LEFT);
	}

	public boolean isTopRight() {
		return contains(SegmentEnum.TOP_RIGHT);
	}

	public boolean isMiddle() {
		return contains(SegmentEnum.MIDDLE);
	}

	public boolean isBottom() {
		return contains(SegmentEnum.BOTTOM);
	}

	public boolean isBottomLeft() {
		return contains(SegmentEnum.BOTTOM_LEFT);
	}

	public boolean isBottomRight() {
		return contains(SegmentEnum.BOTTOM_RIGHT);
	}

	private boolean contains(SegmentEnum segment) {
		return this.segments.contains(segment);
	}

}
