package org.jpimag.segment7.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class Segment7Test {

	@Test
	public void testEmpty() {
		Segment7 segment7 = new Segment7();
		assertFalse(segment7.isTop());
		assertFalse(segment7.isTopLeft());
		assertFalse(segment7.isTopRight());
		assertFalse(segment7.isMiddle());
		assertFalse(segment7.isBottomLeft());
		assertFalse(segment7.isBottomRight());
		assertFalse(segment7.isBottom());
	}

	@Test
	public void testIsTop() {
		Segment7 segment7 = new Segment7(SegmentEnum.TOP);
		assertTrue(segment7.isTop());
	}

	@Test
	public void testIsTopLeft() {
		Segment7 segment7 = new Segment7(SegmentEnum.TOP_LEFT);
		assertTrue(segment7.isTopLeft());
	}

	@Test
	public void testIsTopRight() {
		Segment7 segment7 = new Segment7(SegmentEnum.TOP_RIGHT);
		assertTrue(segment7.isTopRight());
	}

	@Test
	public void testIsMiddle() {
		Segment7 segment7 = new Segment7(SegmentEnum.MIDDLE);
		assertTrue(segment7.isMiddle());
	}

	@Test
	public void testIsBottomLeft() {
		Segment7 segment7 = new Segment7(SegmentEnum.BOTTOM_LEFT);
		assertTrue(segment7.isBottomLeft());
	}

	@Test
	public void testIsBottomRight() {
		Segment7 segment7 = new Segment7(SegmentEnum.BOTTOM_RIGHT);
		assertTrue(segment7.isBottomRight());
	}

	@Test
	public void testIsBottom() {
		Segment7 segment7 = new Segment7(SegmentEnum.BOTTOM);
		assertTrue(segment7.isBottom());
	}
}
