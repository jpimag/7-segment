package org.jpimag.segment7;

import static java.util.Arrays.asList;
import static org.jpimag.segment7.model.Segment7Characters.FIVE;
import static org.jpimag.segment7.model.Segment7Characters.FOUR;
import static org.jpimag.segment7.model.Segment7Characters.HEIGHT;
import static org.jpimag.segment7.model.Segment7Characters.NINE;
import static org.jpimag.segment7.model.Segment7Characters.ONE;
import static org.jpimag.segment7.model.Segment7Characters.SEVEN;
import static org.jpimag.segment7.model.Segment7Characters.SIX;
import static org.jpimag.segment7.model.Segment7Characters.THREE;
import static org.jpimag.segment7.model.Segment7Characters.TWO;
import static org.jpimag.segment7.model.Segment7Characters.ZERO;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import org.jpimag.segment7.model.Segment7;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class Segment7DisplayerTest {

	@ParameterizedTest
	@MethodSource("toDisplayTestProvider")
	public void testToDisplay(List<Segment7> segment7s, String expected) {
		assertEquals(expected, new Segment7Displayer().toDisplay(segment7s));
	}

	@SuppressWarnings("unused")
	private static Stream<Arguments> toDisplayTestProvider() {
		return Stream
				.of(
						Arguments.of(asList(), readExpectation("empty")),
						Arguments.of(asList(ZERO), readExpectation("0")),
						Arguments.of(asList(ONE), readExpectation("1")),
						Arguments.of(asList(TWO), readExpectation("2")),
						Arguments.of(asList(THREE), readExpectation("3")),
						Arguments.of(asList(FOUR), readExpectation("4")),
						Arguments.of(asList(FIVE), readExpectation("5")),
						Arguments.of(asList(SIX), readExpectation("6")),
						Arguments.of(asList(SEVEN), readExpectation("7")),
						Arguments.of(asList(HEIGHT), readExpectation("8")),
						Arguments.of(asList(NINE), readExpectation("9")),
						Arguments
								.of(
										asList(ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, HEIGHT, NINE),
										readExpectation("0123456789")));
	}

	/**
	 * 
	 * @param numbers a list of numbers
	 * @return 7-segment display expectation from resource files
	 */
	private static String readExpectation(String numbers) {
		try {
			return new String(
					Files.readAllBytes(Paths.get(MainTest.class.getResource(numbers).toURI())),
					StandardCharsets.UTF_8).replaceAll("\r\n", "\n");
		} catch (URISyntaxException | IOException e) {
			throw new RuntimeException(e);
		}
	}
}
