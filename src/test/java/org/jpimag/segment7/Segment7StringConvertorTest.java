package org.jpimag.segment7;

import static java.util.Arrays.asList;
import static org.jpimag.segment7.model.Segment7Characters.FIVE;
import static org.jpimag.segment7.model.Segment7Characters.FOUR;
import static org.jpimag.segment7.model.Segment7Characters.HEIGHT;
import static org.jpimag.segment7.model.Segment7Characters.NINE;
import static org.jpimag.segment7.model.Segment7Characters.ONE;
import static org.jpimag.segment7.model.Segment7Characters.SEVEN;
import static org.jpimag.segment7.model.Segment7Characters.SIX;
import static org.jpimag.segment7.model.Segment7Characters.THREE;
import static org.jpimag.segment7.model.Segment7Characters.TWO;
import static org.jpimag.segment7.model.Segment7Characters.ZERO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.stream.Stream;

import org.jpimag.segment7.model.Segment7;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class Segment7StringConvertorTest {

	/**
	 * Test that a bad character input throw an {@link IllegalArgumentException}
	 */
	@Test
	public void testFromStringBadCharacter() {
		assertThrows(IllegalArgumentException.class, () -> new Segment7StringConvertor().fromString("a8"));
	}

	/**
	 * Perform a testcase provided by
	 * {@link Segment7StringConvertorTest#fromStringProvider()}
	 * 
	 * @param expected the expected result
	 * @param number   the input string
	 */
	@ParameterizedTest
	@MethodSource("fromStringProvider")
	public void testFromString(List<Segment7> expected, String number) {
		assertEquals(expected, new Segment7StringConvertor().fromString(number));
	}

	@SuppressWarnings("unused")
	private static Stream<Arguments> fromStringProvider() {
		return Stream
				.of(
						Arguments.of(asList(), ""),
						Arguments.of(asList(ZERO), "0"),
						Arguments.of(asList(ONE), "1"),
						Arguments.of(asList(TWO), "2"),
						Arguments.of(asList(THREE), "3"),
						Arguments.of(asList(FOUR), "4"),
						Arguments.of(asList(FIVE), "5"),
						Arguments.of(asList(SIX), "6"),
						Arguments.of(asList(SEVEN), "7"),
						Arguments.of(asList(HEIGHT), "8"),
						Arguments.of(asList(NINE), "9"),
						Arguments.of(asList(ONE, ZERO), "10"),
						Arguments.of(asList(HEIGHT, SEVEN, ONE), "871"),
						Arguments.of(asList(ZERO, ONE, FOUR), "014"));
	}
}
