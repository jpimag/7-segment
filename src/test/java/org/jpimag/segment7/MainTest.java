package org.jpimag.segment7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Integration test of {@link Main#main(String[])}
 */
public class MainTest {

	@Test
	public void testMainEmpty() throws UnsupportedEncodingException {
		testMain("", readExpectation("empty"));
	}

	@ParameterizedTest
	@MethodSource("mainTestProvider")
	public void testMain(String s, String expected) throws UnsupportedEncodingException {
		// Redirect system out
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));

		// When
		Main.main(new String[] { s });

		// Then
		assertEquals(expected, out.toString(StandardCharsets.UTF_8.name()));
	}

	/**
	 * 
	 * @return tests cases reading expectation from resource files
	 */
	@SuppressWarnings("unused")
	private static Stream<Arguments> mainTestProvider() {
		return Stream
				.of("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0123456789")
				.map(s -> Arguments.of(s, readExpectation(s)));

	}

	private static String readExpectation(String numbers) {
		try {
			return new String(Files.readAllBytes(Paths.get(MainTest.class.getResource(numbers).toURI())), StandardCharsets.UTF_8)
					.replaceAll("\r\n", "\n");
		} catch (URISyntaxException | IOException e) {
			throw new RuntimeException(e);
		}
	}
}
